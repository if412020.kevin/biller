package com.example.billing.controller;

import com.example.billing.entity.Bill;
import com.example.billing.service.MainService;
import java.util.List;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bill-balancer/")
public class MainController {
  private MainService mainService;

  public MainController(MainService mainService) {
    this.mainService = mainService;
  }

  @PostMapping(path = "/add-user")
  public boolean addUser(
      @RequestParam String user) {
    return mainService.addUser(user);
  }

  @PostMapping(path = "/add-bill")
  public boolean addBill(
      @RequestBody Bill bill) {
    return mainService.addBill(bill);
  }

  @PutMapping(path = "/settle")
  public List<String> settle() {
    return mainService.settlement();
  }
}
