package com.example.billing.service;

import com.example.billing.entity.Bill;
import com.example.billing.entity.Debt;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class BillService {
  private List<Bill> bills;
  private List<Debt> debts;

  public BillService() {
    this.bills = new ArrayList<>();
    this.debts = new ArrayList<>();
  }

  public void addBill(Bill bill) {
    bills.add(bill);
    addDebt(bill);
  }

  public List<String> settlement() {
    Map<String, Integer> balance = new HashMap<>();
    List<String> settlement = new ArrayList<>();
    for (Debt debt : debts) {
      if (!balance.containsKey(debt.getFrom())) {
        balance.put(debt.getFrom(), 0);
      }
      if (!balance.containsKey(debt.getTo())) {
        balance.put(debt.getTo(), 0);
      }

      balance.put(debt.getFrom(), balance.get(debt.getFrom()) - debt.getAmount());
      balance.put(debt.getTo(), balance.get(debt.getTo()) + debt.getAmount());
    }

    for (Entry<String, Integer> userBalance : balance.entrySet()) {
      // check any user which amount is > 0
      if (userBalance.getValue()>0) {
        // need to backtrack who had debt to last user
        List<String> debtors = getDebtPayerOfUser(userBalance.getKey());


        for (String debtor : debtors) {
          int debtorBalance = balance.get(debtor);
          if (debtorBalance < 0) {
            int amountPaid = userBalance.getValue() > debtorBalance*-1 ?  debtorBalance*-1 : userBalance.getValue();
            // add amount of the remaining from debter(which point is minus)
            balance.put(debtor, debtorBalance + amountPaid);
            // substrack the balance of the payer
            balance.put(userBalance.getKey(), balance.get(userBalance.getKey()) - amountPaid);
            // add to settlement list
            settlement.add(debtor + " PAID "+ amountPaid +" TO " + userBalance.getKey());
          }
        }

      }
    }

    bills.clear();
    debts.clear();
    return settlement;
  }

  public void billInfo() {
    System.out.println(bills.size());
    System.out.println(bills);
  }

  public void debtInfo() {
    System.out.println(debts.size());
    System.out.println(debts);
  }

  private void addDebt(Bill bill) {
    if (bill.isSplitBill()) {
      int splitAmount = bill.getAmount()/(bill.getBillAbleGroup().size()+1);
      bill.getBillAbleGroup().stream()
          .forEach(billAbleUser -> debts.add(Debt.builder()
              .from(billAbleUser)
              .to(bill.getPayer())
              .amount(splitAmount)
              .build()));
    } else {
      debts.add(Debt.builder()
          .from(bill.getBillAbleUser())
          .to(bill.getPayer())
          .amount(bill.getAmount())
          .build());
    }
  }

  private List<String> getDebtPayerOfUser(String user) {
    return debts.stream()
        .filter(debt -> debt.getTo().equalsIgnoreCase(user))
        .map(Debt::getFrom)
        .collect(Collectors.toList());
  }
}
