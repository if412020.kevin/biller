package com.example.billing.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class UserService {
  private List<String> users;
  private static final int max = 100;

  public UserService() {
    this.users = new ArrayList<>();
    users.add("a");
    users.add("b");
    users.add("c");
  }

  public List<String> getUsers() {
    return users;
  }

  public boolean addUsers(String user) {
    if (max == users.size()) {
      return false;
    } else {
      users.add(user);
      return true;
    }
  }

  public boolean isUserExist(String user) {
    return users.contains(user);
  }

  public boolean isUserExist(List<String> user) {
    for (String user1 : user) {
      if (!users.contains(user1))
        return false;
    }
    return true;
  }

  public void groupInfo() {
    System.out.println(users.size());
    System.out.println(users);
  }
}
