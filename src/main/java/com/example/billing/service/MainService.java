package com.example.billing.service;

import com.example.billing.entity.Bill;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MainService {
  private BillService billService;
  private UserService userService;

  @Autowired
  public MainService(BillService billService, UserService userService) {
    this.billService = billService;
    this.userService = userService;
  }

  public boolean addUser(String user) {
    boolean result = userService.addUsers(user);
    userService.groupInfo();
    return result;
  }

  public boolean addBill(Bill bill) {
    if (!checkAllUserExist(bill)) {
      return false;
    }
    billService.addBill(bill);
    billService.billInfo();
    billService.debtInfo();
    return true;
  }

  public List<String> settlement() {
    return billService.settlement();
  }

  private boolean checkAllUserExist(Bill bill) {
    if (bill.isSplitBill()) {
      return userService.isUserExist(bill.getPayer()) && userService.isUserExist(bill.getBillAbleGroup());
    } else {
      return userService.isUserExist(bill.getPayer()) && userService.isUserExist(bill.getBillAbleUser());
    }
  }
}
