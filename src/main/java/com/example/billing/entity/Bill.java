package com.example.billing.entity;

import java.util.List;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bill {
  private int amount;
  private String payer;
  private String billAbleUser;
  private List<String> billAbleGroup;

  public boolean isSplitBill() {
    return Objects.nonNull(billAbleGroup) && !billAbleGroup.isEmpty();
  }
}
