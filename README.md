# biller

biller, using spring boot

There are 3 API for biller :

- **INSERT NEW USER**

`POST localhost:8080/bill-balancer/add-user/?user=kevin`

will return boolean

`user` field as param of username



- **ADD BILL**

`POST localhost:8080/bill-balancer/add-bill/`
will return boolean

this implies, user A is in debt to user C with amount 15000
```
{
	"amount": 15000,
	"payer": "C",
	"billAbleUser": "A"
}
```

this is for split bill request, where A and B is in debt to user C with amount 15000/3
```
{
	"amount": 15000,
	"payer": "C",
	"billAbleGroup": [
		"A",
		"B"
	]
}
```


- **SETTLEMENT**

`PUT localhost:8080/bill-balancer/settle/`
will do calculation for settlement, 

and remove all previous transaction

Response body
```
[
  "a PAID 4000 TO b",
  "a PAID 2000 TO c"
]
```
